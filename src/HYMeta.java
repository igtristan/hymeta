
import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.util.ServerRunner;

import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.text.*;
import java.net.*;


public class HYMeta extends NanoHTTPD 
{
	public static String PREFIX          = "";
	public static int STATS_PORT         = 80;
	public static boolean s_debug        = false;
	public static HYMetaFetcher s_data   = null;
	public static String s_time_start   = "";
	
    private static class HYMetaSource
    {
    	String url;
    	String status;
    	
    	public HYMetaSource(String url) {
    		this.url    = url;
    		this.status = "loading";
    	}
    }
    
	private static class HYMetaFetcher extends Thread
	{
		public ArrayList<HYMetaSource> m_url = new ArrayList<HYMetaSource>();
		
		public Hashtable<String,String> m_data = new Hashtable<String,String>();
		public HYMetaFetcher(ArrayList<String> url) {
			for (String s: url) {
				m_url.add(new HYMetaSource(s));
			}
			appendStatus(m_data);
			
			attemptDownload();
			this.start();
		}
		
		public String lookup(String key) {
			if (m_data.containsKey(key)) {
				return m_data.get(key);
			}
			else {
				return null;
			}
		}
		
		public void run() 
		{
			while(true) {
				try
				{
					attemptDownload();
					
					// update every 5 minutes
					Thread.sleep(6*60*1000);
				}
				catch (InterruptedException ex) {
					ex.printStackTrace();
				}
			}
		}
		
		public void appendStatus(Hashtable<String,String> h) {
		
			{
				StringBuilder sb = new StringBuilder();
				sb.append("{");
				sb.append("\"status\": \"ok\",");
				sb.append("\"time-start\":\"" + s_time_start + "\",");
				sb.append("\"sources\": {");
				boolean first = true;
				for (HYMetaSource url : m_url) {
					if (!first) {
						sb.append(",");
					}
					first = false;
					sb.append("\"" + url.url + "\": \"" + url.status + "\"");
					
				}
				sb.append("}");
				sb.append("}");
				

				h.put("/status", sb.toString());
			}
		}

		public void attemptDownload() 
		{
			Hashtable<String,String> new_hash = new Hashtable<String,String>();
			boolean found_one = false;
			for (int i = 0; i < m_url.size(); i++)
			{
				HYMetaSource url    = m_url.get(i);
				
				try
				{
					URL oracle = new URL(url.url);
					BufferedReader in = new BufferedReader(
					new InputStreamReader(oracle.openStream()));

					StringBuilder acc = new StringBuilder();
					String inputLine;
					String section = null;
					
					
					
					while ((inputLine = in.readLine()) != null)
					{
						if (inputLine.startsWith("#")) {
							continue;
						}
						else if (inputLine.startsWith("@include")) 
						{
							String url_append = inputLine.substring("@include".length()).trim();
							String new_url = url.url.substring(0, url.url.lastIndexOf("/") + 1) + url_append;
							
							// check to see if this include is already in the list
							boolean found = false;
							for (int j = 0; j < m_url.size(); j++) {
								if (m_url.get(j).url.equals(new_url)) {
									found = true;
									break;
								}
							}
							
							// nope it isn't.  Add as a new url source
							if (!found) {
								m_url.add(new HYMetaSource(new_url));
							}
						}
						else if (inputLine.startsWith("@")) {
							if (section != null) {
								new_hash.put(section, acc.toString());
							}
					
							section = inputLine.substring(1);
							acc     = new StringBuilder();
						}
						else if (section != null) {					
							acc.append(inputLine);					
						}
					}
					if (section != null) {
						new_hash.put(section, acc.toString());
					}

					in.close();
					
					url.status = "success";
					found_one = true;
				}
				catch (IOException ex) {
					url.status = "failed. ";
					ex.printStackTrace();
				}
				
				
			}
			
			if (found_one) 
			{
				synchronized (this) {
					appendStatus(new_hash);
					m_data = new_hash;
				}
			}
			else {
				synchronized (this) {
					appendStatus(m_data);
				}
			}
		}
	
	}
	
    public HYMeta(int port) 
    {
        super(port);
    }


    public static void main(String[] args) 
    {	
    	// check if there are any command line arguments
    	//////////////////////////////////////////////////
		STATS_PORT         = 80;
		PREFIX             = "";
		ArrayList<String> urls = new ArrayList<String>();

		for (int i = 0; i < args.length; i++) {
			if (args[i].equals("-p")) {
				i++;
				STATS_PORT = Integer.parseInt(args[i]);
				System.out.println("port: "  + args[i]);
			}
			if (args[i].equals("-prefix")) {
				i++;
				PREFIX = args[i];
				System.out.println("prefix: " + args[i]);
			}
			else if (args[i].equals("-u")) {
				i++;
				urls.add(args[i]);
				System.out.println("url: "    + args[i]);
			}
		}
		
		
		TimeZone tz = TimeZone.getTimeZone("UTC");
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm'Z'");
		df.setTimeZone(tz);
		s_time_start = df.format(new Date());

		// check if there is an environment variable containing the source urls		
		//////////////////////////////////////////////////////////////
		{
			// just also append our env variables for the current time
			Map<String, String> env = System.getenv();
			if (env.containsKey("HYMETA_SOURCE_URLS")) {
				String [] value = env.get("HYMETA_SOURCE_URLS").split(",");
				for (String v : value) {
					urls.add(v);
				}
			}
		}

		s_data = new HYMetaFetcher(urls);

		HYMeta server     = new HYMeta(STATS_PORT);	
		try {
			server.start();

			long start = System.currentTimeMillis();
			Thread.sleep(100L);
			while (!server.wasStarted()) {
				Thread.sleep(100L);
				if (System.currentTimeMillis() - start > 2000) 
				{
					//Assert.fail("could not start server");
				}
			}
		}
		catch (InterruptedException e) 
		{
			e.printStackTrace();
		}
		catch (IOException ioe) 
		{
			System.err.println("Couldn't start server:\n" + ioe);
			System.exit(-1);
		}
		
		if (args.length > 0 && args[0].equals("--prompt"))
		{
			System.out.println("Server started, Hit Enter to stop.\n");

			try {
				System.in.read();
			} catch (Throwable ignored) {
			}

			server.stop();
			System.out.println("Server stopped.\n");
		}        
    }
    
	@Override public void stop()
	{
		super.stop();
	}

    @Override public Response serve(IHTTPSession session) 
    {
        StringBuilder sb = new StringBuilder();   
            
        try
        {
        	Map<String, String> body = new Hashtable<String,String>();
        
        	if (s_debug) {
        		System.out.println("Message received: " + session.getUri());
			}
			
			// get the URI being accessed.
			// these will always start with a "/"		
			String uri = String.valueOf(session.getUri());

			// remove the prefix.  This is to play nicely
			// with the AWS application load balancer
			if (uri.startsWith(PREFIX)) {
				uri = uri.substring(PREFIX.length());
			}
	
			if (uri.equals("/ping.html")) {
				return newFixedLengthResponse("pong");
			}
			
			String value = s_data.lookup(uri);
			if (value == null) {
				return newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_PLAINTEXT, "{\"status\":\"error\",\"message\":\"not found. URI: " + uri +"\"}");
			}
			else {
			 	return newFixedLengthResponse(value);
			}
		}
		catch (RuntimeException ex)
		{
			return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT,
				 "{\"status\":\"error\",\"message\":\"exception"   + ex.getMessage() +  "\"}");
		}
		catch (Exception ex)
		{
			return newFixedLengthResponse(Response.Status.INTERNAL_ERROR, NanoHTTPD.MIME_PLAINTEXT, 
				"{\"status\":\"error\",\"message\":\"exception"   + ex.getMessage() +  "\"}");
		}
    }
}
