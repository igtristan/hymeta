#!/bin/sh

main()
{
   [ -d jar ] || mkdir jar


	cd src
	 
	# delete all existing class files
    find . -name *.class -exec rm -rf {} \;
    
    # compile
	javac  HYMeta.java || exit 1

	# create the jar
   
	jar cvfe ../jar/hymeta.jar HYMeta .
	cd ..
	
	cp ./jar/hymeta.jar ./hymeta.jar
}

main;
