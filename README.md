###HYMeta

HYMeta is a Java based meta data server.  
It serves JSON responses over HTTP on port 80.
Environment variable HYMETA_SOURCE_URLS specifies a comma separated list of URLS to
pull its configuration from.

##COMMAND LINE

*java -jar hymeta.jar*

OPTIONS:
-p       XX - specify a unique port for the server to run on
-prefix  YY - specify a prefix to be removed from incoming URIs

eg.  -prefix /somefolder  for a URI  /somefolder/somedocument
	remove "/somefolder" from the beginning of the URI, and would search the metadata 
	archive for "/somedocument" instead
	
-u		ZZ - specifies a URL to pull the configuration data for the server
This is appended to any URLS found in the environment variable HYMETA_SOURCE_URLS

##CONFIG FILE FORMAT

Each file contains either comments, includes, or keys followed by blocks of data.


#Data

Each block of data is started by an '@' on a new line followed by the key.
The following two examples would be accessed at the following urls.
http://myserver/mykey1  and
http://myserver/mykey2/more
```
@/mykey1
{
	"some":  { "json": "data" }
}

@/mykey2/more
{
	"more":  {"json" : 22 }
}
```
#Comments
```
#This is a comment
```
#Includes
```
@include this.is.a.file.in.the.same.directory.txt
```